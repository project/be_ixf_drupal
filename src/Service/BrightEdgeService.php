<?php

namespace Drupal\brightedge\Service;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Provides BrightEdgeService.
 */
class BrightEdgeService implements BrightedgeServiceInterface {

  /**
   * The config service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */

  protected $config;

  /**
   * The $database service.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * The $language service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new BrightEdgeService.
   *
   * @param object $config
   *   The config manager.
   * @param Drupal\Core\Database\Driver\mysql\Connection $database
   *   The database manager.
   * @param Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct($config, Connection $database, LanguageManagerInterface $languageManager) {
    $this->config = $config->get('be_ixf_drupal.settings');
    $this->languageConfig = $config->get('be_ixf_drupal.locales');
    $this->database = $database;
    $this->languageManager = $languageManager;
  }

}
