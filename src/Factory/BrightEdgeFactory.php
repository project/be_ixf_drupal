<?php

namespace Drupal\be_ixf_drupal\Factory;

use GuzzleHttp\ClientInterface;
use BrightEdge\BEIXFClient;

/**
 * Class BrightEdgeFactory.
 */
class BrightEdgeFactory {

  /**
   * {@inheritdoc}
   */
  public static function createRequest($config, ClientInterface $client) {
    $be_config = $config->get('be_ixf_drupal.settings');

    $be_ixf_config = [
      BEIXFClient::$CAPSULE_MODE_CONFIG => $be_config->get('capsule_mode'),
      BEIXFClient::$ACCOUNT_ID_CONFIG => $be_config->get('account_id'),
    ];
    $api_endpoint = $be_config->get('api_endpoint');
    if (!empty($api_endpoint)) {
      $be_ixf_config[BEIXFClient::$API_ENDPOINT_CONFIG] = $api_endpoint;
    }
    $be_ixf_config['defer.redirect'] = "true";
    return new BrightEdgeIXFPHPClient($be_ixf_config);
  }

}

/**
 * {@inheritdoc}
 */
class BrightEdgeIXFPHPClient {
  // Subscriber and block instaniate different instances.
  // From factory so we use static array here.

  /**
   * Static variable $sdkClientArray.
   *
   * @var sdkClientArray
   */
  protected static $sdkClientArray = [];

  /**
   * Variable $sdkConfig.
   *
   * @var sdkConfig
   */
  protected $sdkConfig;

  /**
   * Constructs a new BrightEdgeIXFPHPClient.
   *
   * @param object $sdkConfig
   *   The config manager.
   */
  public function __construct($sdkConfig) {
    $this->sdkConfig = $sdkConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if (array_key_exists($url, self::$sdkClientArray)) {
      $be_ixf_client = self::$sdkClientArray[$url];
    }
    else {
      self::$sdkClientArray = [];
      $be_ixf_client = new BEIXFClient($this->sdkConfig);
      self::$sdkClientArray[$url] = $be_ixf_client;
    }

    return $be_ixf_client;
  }

}
