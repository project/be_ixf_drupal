<?php

namespace Drupal\be_ixf_drupal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DrupalAdminForm.
 *
 * @package Drupal\be_ixf_drupal\Form
 */
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'be_ixf_drupal_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [BE_IXF_DRUPAL];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(BE_IXF_DRUPAL);

    $form[CAPSULE_MODE] = [
      TYPE => 'select',
      TITLE => $this->t('Capsule Mode'),
      DESCRIPTION => $this->t('Mode to use for connecting to BrightEdge. Leave as default.'),
      '#options' => [
        'REMOTE_PROD_CAPSULE_MODE' => $this->t('Production'),
        'REMOTE_PROD_GLOBAL_CAPSULE_MODE' => $this->t('Production Global'),
      ],
      DEFAULT_VALUE => $config->get(CAPSULE_MODE),
    ];

    $form['account_id'] = [
      TYPE => TEXTFIELD,
      TITLE => $this->t('Account Id'),
      DESCRIPTION => $this->t('Account configured in the f000000ZZZ form'),
      '#size' => 20,
      '#maxlength' => 20,
      '#required' => TRUE,
      DEFAULT_VALUE => $config->get(ACCOUNT_ID) !== NULL ? $config->get(ACCOUNT_ID) : '',
    ];

    $form['api_endpoint'] = [
      TYPE => TEXTFIELD,
      TITLE => $this->t('API Endpoint'),
      DESCRIPTION => $this->t('API Endpoint in https://ixfN-api.bc0a.com'),
      DEFAULT_VALUE => $config->get(API_ENDPOINT) !== NULL ? $config->get(API_ENDPOINT) : '',
    ];

    $form['block_cache_max_age'] = [
      TYPE => TEXTFIELD,
      TITLE => $this->t('Block Cache Maximum Age'),
      '#size' => 4,
      '#maxlength' => 6,
      DESCRIPTION => $this->t('The maximum age cache time for the block in seconds'),
      DEFAULT_VALUE => $config->get(BLOCK_CACHE_MAX_AGE) !== NULL ? $config->get(BLOCK_CACHE_MAX_AGE) : '3600',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!is_numeric($values[BLOCK_CACHE_MAX_AGE])) {
      form_set_error(BLOCK_CACHE_MAX_AGE, t('You must enter an integer for block cache maximum age.'));
    }
    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $account_id = trim($values[ACCOUNT_ID]);
    $api_endpoint = trim($values[API_ENDPOINT]);

    $this->configFactory()->getEditable(BE_IXF_DRUPAL)
      ->set(CAPSULE_MODE, $values[CAPSULE_MODE])
      ->set(ACCOUNT_ID, $account_id)
      ->set(API_ENDPOINT, $api_endpoint)
      ->set(BLOCK_CACHE_MAX_AGE, $values[BLOCK_CACHE_MAX_AGE])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
