<?php

namespace Drupal\be_ixf_drupal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'BrightEdge Foundation Content' Block.
 *
 * @Block(
 *   id = "ixf_content_block",
 *   admin_label = @Translation("BrightEdge Foundation Content Block"),
 *   category = @Translation("BrightEdge Foundation Content Block"),
 * )
 */
class IXFContentBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form[BODY_TYPE] = [
      TYPE => 'select',
      TITLE => $this->t('Type'),
      DESCRIPTION => $this->t('The type of content block'),
      '#options' => [
        BODY_OPEN => BODY_OPEN,
        'body_1' => $this->t('body_1'),
        OTHER_BODY_TYPE => OTHER_BODY_TYPE,
      ],
      DEFAULT_VALUE => isset($config[BODY_TYPE]) ? $config[BODY_TYPE] : 'BODYSTR',
    ];

    $form[FEATURED_GROUP] = [
      TYPE => 'textfield',
      TITLE => $this->t('Feature Group Id'),
      DESCRIPTION => $this->t('The Id of the content block'),
      '#states' => [
        'visible' => [
          'select[name="settings[body_type]"]' => ['value' => OTHER_BODY_TYPE],
        ],
      ],
      DEFAULT_VALUE => isset($config[FEATURED_GROUP]) ? $config[FEATURED_GROUP] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function baseConfigurationDefaults() {
    // Not in default drupal.
    // See @https://www.drupal.org/project/drupal/issues/2911733.
    // Make default title not visible.
    $defaults = parent::baseConfigurationDefaults();
    if (is_array($defaults) && isset($defaults['label_display'])) {
      $defaults['label_display'] = '';
    }
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration[BODY_TYPE] = $form_state->getValue(BODY_TYPE);
    $this->configuration[FEATURED_GROUP] = $form_state->getValue(FEATURED_GROUP);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    // Only apply for nodes not admin or user.
    if (!isset($node)) {
      return NULL;
    }
    // Internal Page Cache module causes problems which.
    // Makes the module cached forever.
    // Drupal ticket; https://www.drupal.org/node/2352009.
    $debug = \Drupal::request()->query->get('ixf-debug');
    $config = $this->getConfiguration();
    $node_type = $config[BODY_TYPE];
    if ($node_type == OTHER_BODY_TYPE) {
      $node_type = $config[FEATURED_GROUP];
    }
    $be_ixf_client = \Drupal::service("brightedge.request")->getClient();
    $raw_html = "";
    if ($node_type == BODY_OPEN) {
      $raw_html .= $be_ixf_client->getBodyOpen();
      if ($debug) {
        $raw_html .= $be_ixf_client->close();
      }
    }
    else {
      $raw_html .= $be_ixf_client->getBodyString($node_type);
      if ($debug) {
        $raw_html .= $be_ixf_client->close();
      }
    }
    $build['autopilot_block'] = [
      '#theme' => 'ixf_block',
      '#body_string' => $raw_html,
      '#cache' => [
        'max-age' => $this->getCacheMaxAge(),
        'tags' => $this->getCacheTags(),
        'contexts' => $this->getCacheContexts(),
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // @see https://www.drupal.org/docs/8/api/cache-api/cache-tags.
    // With this when your node change your block will rebuild.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      // If there is node add its cachetag.
      $config = $this->getConfiguration();
      $node_type = $config[BODY_TYPE];
      $node_feature_group = "";
      if (!empty($config[FEATURED_GROUP])) {
        $node_feature_group = $config[FEATURED_GROUP];
      }

      if ($node instanceof \Drupal\node\NodeInterface) {
        return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id(), 'custom_be:' . $node->id() . "_" . $node_type . "_" . $node_feature_group]);
      } else {
        return parent::getCacheTags();
      }
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @see https://www.drupal.org/developing/api/8/cache/contexts.
    // If you depends on \Drupal::routeMatch().
    // You must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path', 'url.query_args']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // This doesn't work for logged out users.
    // @see https://www.drupal.org/project/drupal/issues/2592555.
    // @see https://www.drupal.org/project/drupal/issues/2352009.
    $module_config = \Drupal::config('be_ixf_drupal.settings');
    if ($module_config->get(BLOCK_CACHE_MAX_AGE) != NULL) {
      $cache_age = intval($module_config->get(BLOCK_CACHE_MAX_AGE));
    }
    return $cache_age;
  }

}
