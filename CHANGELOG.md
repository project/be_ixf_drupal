# Changelog

## [Unreleased]

## [1.2.5] - 2020-08-25
### Fix
- Node Revision view issue

## [1.2.4] - 2020-07-29
### Fix
- Node Revision issue

## [1.2.3] - 2020-07-27
### Fix
- sonarqube codesmell fix

## [1.2.2] - 2020-07-15
### Fix
- Coding Standard fix

## [1.2.1] - 2020-07-06
### Added
- Making Accound id as required

## [1.2.0] - 2020-06-23
### Fixed
- Removing Notices

## [1.0-rc2] - 2020-06-10
### Fixed
- Head String Fix to be in <head> region

## [1.0-beta12] - 2019-01-17
### Fixed
- Missed settings.xml file fix.  Changed default endpoint as well

## [1.0-beta11] - 2019-01-09
### Fixed
- Make settings match plugin name.  Please do not upgrade to this version
from prior to 1.0-beta10 without contacting support.
- If you do upgrade manually please goto Configuration->BrightEdge to
configure your org id, endpoint, and cache. The BrightEdge Foundation
- Content Blocks will remain in place.
- Changed the admin form to show new domain

## [1.0-beta10] - 2019-01-08
### Added
- Clarify autoload and addresses warnings

## [0.5.0] - 2018-02-28
### Added
- Fix up notices and warnings

## [0.4.1] - 2018-02-09
### Added
- Public beta 4 version with redirect node fix

## [0.3.1] - 2018-01-31
### Added
- Public beta 3 version with redirect node caching
